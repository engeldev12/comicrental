class Comic
  StateIsNotValid = Class.new(StandardError)
  PriceIsNotValid = Class.new(StandardError)
  NameIsNotValid = Class.new(StandardError)
  
  def self.with(name:,price:,state:)
    new(name: name, price: price, state: state)
  end

  def state
    @state.value
  end

  def price
    @price.value
  end

  def name
    @name.value
  end

  private
   def initialize(name:, price:, state:)
     @name = ComicName.with value: name
     @price = ComicPrice.with value: price
     @state = ComicState.with value: state
   end
end


class ComicState
  attr_reader :value
  VALID_STATE = [
    :excelent,
    :good,
    :acceptable,
    :impaired,
    :damaged
  ]
 
  def self.with(value:)
    raise Comic::StateIsNotValid unless VALID_STATE.include? value
    new(value: value)
  end

  private
    def initialize(value:)
      @value = value
    end
end


class ComicPrice
  attr_reader :value
 
  def self.with(value:)
    raise Comic::PriceIsNotValid, 'It is not a number' unless value.is_a? Numeric
    raise Comic::PriceIsNotValid if value < 0
    new(value: value)
  end

  private
    def initialize(value:)
      @value = value
    end
end


class ComicName
  attr_reader :value
 
  def self.with(value:)
    if value.nil? or not value.is_a? String
      raise Comic::NameIsNotValid 
    end
    new(value: value)
  end

  private
    def initialize(value:)
      @value = value
    end
end
