class Discount
  attr_reader :comic, :percentage_of_discount
  
  def self.for(comic:)
    new(comic: comic, discount: @percentage_of_discount)
  end

  def apply
    Discount.discount(price: @comic.price, percent: @percentage_of_discount)
  end
  
  private
    def self.discount(price:, percent:)
      price - self.percentage(price: price, percent: percent)
    end

    def self.percentage(price:,percent:)
      ((price * percent) / 100)
    end

    def initialize(comic:, discount:)
      @comic = comic
      @percentage_of_discount = discount
    end
end

class ExcelentComicDiscount < Discount
  @percentage_of_discount = 10
end

class GoodComicDiscount < Discount
  @percentage_of_discount = 20
end

class AcceptableComicDiscount < Discount
  @percentage_of_discount = 25
end

class ImpairedComicDiscount < Discount
  @percentage_of_discount = 30
end

class DamagedComicDiscount < Discount
  @percentage_of_discount = 50
end
