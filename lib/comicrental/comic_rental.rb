class ComicRental
  ThisIsNotAComic = Class.new(TypeError)

  def self.cost(comic)
    raise ThisIsNotAComic unless comic.is_a? Comic

    case comic.state
    when :excelent
      ExcelentComicDiscount.for(comic: comic).apply
    when :good
      GoodComicDiscount.for(comic: comic).apply
    when :acceptable
      AcceptableComicDiscount.for(comic: comic).apply
    when :impaired
      ImpairedComicDiscount.for(comic: comic).apply
    when :damaged
      DamagedComicDiscount.for(comic: comic).apply
    end
  end
end
