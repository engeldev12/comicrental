require 'test_helper'


describe ComicRental do
  describe 'cuando una historieta está excelente' do
    it 'tendrá un descuento del 10%' do
      @comic = Comic.with name: "Naruto", price: 20.0, state: :excelent
      expect(ComicRental.cost(@comic)).must_equal 18.0
    end
  end

  describe 'cuando una historieta está buena' do
    it 'tendrá un descuento del 20%' do
      @comic = Comic.with name: "One Punch Man", price: 20.0, state: :good
      expect(ComicRental.cost(@comic)).must_equal 16.0
    end
  end

  describe 'cuando una historieta está aceptable' do
    it 'tendrá un descuento del 25%' do
      @comic = Comic.with name: "DBS", price: 20.0, state: :acceptable
      expect(ComicRental.cost(@comic)).must_equal 15.0
    end
  end

  describe 'cuando una historieta está deteriorada' do
    it 'tendrá un descuento del 25%' do
      @comic = Comic.with name: "Dr. Stone", price: 20.0, state: :impaired
      expect(ComicRental.cost(@comic)).must_equal 14.0
    end
  end

  describe 'cuando una historieta está dañada' do
    it 'tendrá un descuento del 25%' do
      @comic = Comic.with name: "Dr. Stone", price: 20.0, state: :damaged
      expect(ComicRental.cost(@comic)).must_equal 10.0
    end
  end

  describe '.cost' do
    describe 'cuando intento pasar algo que no es un comic' do
      it 'debería error' do
        expect {
          ComicRental.cost(nil)
        }.must_raise(ComicRental::ThisIsNotAComic)

        expect {
          ComicRental.cost("This is not a comic")
        }.must_raise(ComicRental::ThisIsNotAComic)

        expect {
          ComicRental.cost(12)
        }.must_raise(ComicRental::ThisIsNotAComic)
      end
    end
  end
end
