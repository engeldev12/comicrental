require 'test_helper'


describe Comic do
  describe :state do
    describe 'cuando no sea: :excelent, :good, :acceptable, :impaired, :damaged' do
      it 'debería dar error' do
        expect {
          Comic.with name: "I'm", price: 55.3, state: :invalid
        }.must_raise Comic::StateIsNotValid

        expect {
          Comic.with name: "I'm", price: 55.3, state: :excelen
        }.must_raise Comic::StateIsNotValid

        expect {
          Comic.with name: "I'm", price: 55.3, state: nil
        }.must_raise Comic::StateIsNotValid

        expect {
          Comic.with name: "I'm", price: 55.3, state: "excelent"
        }.must_raise Comic::StateIsNotValid
      end
    end
  end

  describe Comic, :price do
    describe 'cuando sea negativo' do
      it 'debería dar error' do
        expect {
          Comic.with name: "Price", price: -12.9, state: :good
        }.must_raise Comic::PriceIsNotValid
      end
    end

    describe 'cuando no sea un número' do
      it 'debería dar error' do
        expect {
          Comic.with name: "Price", price: nil, state: :good
        }.must_raise Comic::PriceIsNotValid, 'It is not a number'

        expect {
          Comic.with name: "Price", price: "212", state: :good
        }.must_raise Comic::PriceIsNotValid, 'It is not a number'
      end
    end
  end

  describe :name do
    describe 'cuando sea nulo' do
      it 'debería dar error' do
        expect {
          Comic.with name: nil, price: 100, state: :good
        }.must_raise Comic::NameIsNotValid
      end
    end

    describe 'cuando no sea un string' do
      it 'debería dar error' do
        expect {
          Comic.with name: 123, price: 100, state: :good
        }.must_raise Comic::NameIsNotValid

        expect {
          Comic.with name: :symbol, price: 100, state: :good
        }.must_raise Comic::NameIsNotValid

        expect {
          Comic.with name: StandardError, price: 100, state: :good
        }.must_raise Comic::NameIsNotValid
      end
    end
  end
end
